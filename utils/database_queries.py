CATEGORIES_TABLE = """
                    CREATE TABLE IF NOT EXISTS categories(
                    id INT AUTO_INCREMENT PRIMARY KEY,
                    categoryName VARCHAR(100) NOT NULL
                    );
                """
PRODUCTS_TABLE = """
                    CREATE TABLE IF NOT EXISTS products(
                    id INT AUTO_INCREMENT PRIMARY KEY,
                    productName varchar(100) NOT NULL,
                    price FLOAT NOT NULL,
                    description varchar(100) NOT NULL,
                    category INT NOT NULL,
                    CONSTRAINT fk_category
                    FOREIGN KEY (category) 
                    REFERENCES categories(id)
                    ON DELETE CASCADE
                    ON UPDATE CASCADE
                    );
                """

CART_TABLE = """
                CREATE TABLE IF NOT EXISTS cart(
                id INT AUTO_INCREMENT PRIMARY KEY,
                productId INT,
                CONSTRAINT fk_products
                FOREIGN KEY (productId) 
                REFERENCES products(id)
                );
            """

BILLING_TABLE = """
                    CREATE TABLE IF NOT EXISTS billing(
                    id INT AUTO_INCREMENT PRIMARY KEY,
                    username VARCHAR(100) NOT NULL,
                    dt_created datetime DEFAULT CURRENT_TIMESTAMP,
                    particulars json NOT NULL,
                    actual_amount FLOAT NOT NULL,
                    discount_amount FLOAT NOT NULL,
                    final_amount FLOAT NOT NULL
                    );
                """
