import mysql.connector
from mysql.connector import errorcode


def connection(host, user, password, port, database=None):
    db_connection = mysql.connector.connect(
        host=host,
        user=user,
        password=password,
        port=port,
        autocommit=True
    )
    if database:
        db_connection.database = database
    return db_connection


def add_table(query, cnx):
    try:
        cursor = cnx.cursor()
        cursor.execute(query)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("table already exists.")
        else:
            print(err.msg)
