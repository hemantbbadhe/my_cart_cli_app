import unittest

from my_cart_app.app.categories import CategoryDBManager
from my_cart_app.app.product import ProductDBManager, ProductUtil
from test.test_db import MockDB
import json


class TestProduct(MockDB):
    def setUp(self) -> None:
        self.db_manager = ProductDBManager(self.cnx)
        self.category_db_manager = CategoryDBManager(self.cnx)

    def test_create_product_success(self):
        self.category_db_manager.add_product_category('Product-category-1')

        validation = ProductUtil(self.cnx).validate_product_data(1, "Test Product", 199.00)
        self.assertTrue(True, validation)

        product_obj = self.db_manager.add_new_product(1, "Test Product", 199.00,
                                                      json.dumps({"suitable_for": "Test Demo"}))
        self.assertEqual(product_obj, "New product added successfully.")

    def test_create_existing_product(self):
        self.category_db_manager.add_product_category('Product-category-1')

        validation = ProductUtil(self.cnx).validate_product_data(1, "Test Product", 199.00)
        self.assertTrue(True, validation)

        product_obj = self.db_manager.add_new_product(1, "Test Product", 199.00,
                                                      json.dumps({"suitable_for": "Test Demo"}))

        validation = ProductUtil(self.cnx).validate_product_data(1, "Test Product", 199.00)
        self.assertEqual(validation, "Product exist in database")

    def test_create_product_for_non_existing_category(self):
        self.category_db_manager.add_product_category('Product-category-1')
        # here 5 is the category id which is not exists in db
        validation = ProductUtil(self.cnx).validate_product_data(1, "Test Demo Product", -199.00)
        self.assertEqual(validation, "Invalid product price")


# if __name__ == '__main__':
#     unittest.main()
