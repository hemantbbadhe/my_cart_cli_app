from my_cart_app.app.categories import CategoryDBManager
from test.test_db import MockDB
import unittest


class TestCategories(MockDB):
    def setUp(self) -> None:
        self.db_manager = CategoryDBManager(self.cnx)

    def test_create_existing_category(self):
        self.db_manager.add_product_category('Product-category-1')
        self.db_manager.add_product_category('Product-category-1')

        product_categories = self.db_manager.get_product_categories()
        self.assertNotEqual(len(product_categories), 2)

    def test_create_new_category_success(self):
        self.db_manager.add_product_category('Product-category-1')

        product_categories = self.db_manager.get_product_categories()
        self.assertEqual(len(product_categories), 1)

    def test_get_categories(self):
        self.db_manager.add_product_category('Product-category-1')
        self.db_manager.add_product_category('Product-category-2')

        product_categories = self.db_manager.get_product_categories()
        self.assertEqual(len(product_categories), 2)

    def test_get_category_by_id_success(self):
        self.db_manager.add_product_category('Product-category-1')
        obj = self.db_manager.get_product_category_by_id(1)
        self.assertEqual(obj[0].get('id'), 1)

    def test_get_category_by_id_fail(self):
        self.db_manager.add_product_category('Product-category-1')
        # sending wrong category id
        obj = self.db_manager.get_product_category_by_id(5)
        self.assertNotEqual(obj, 5)


if __name__ == '__main__':
    unittest.main()
