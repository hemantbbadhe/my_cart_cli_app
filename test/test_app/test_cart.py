import json
import unittest

from my_cart_app.app.cart import CartDBManager
from my_cart_app.app.categories import CategoryDBManager
from my_cart_app.app.product import ProductDBManager, ProductUtil
from test.test_db import MockDB


class TestCart(MockDB):
    def setUp(self) -> None:
        self.db_manager = CartDBManager(self.cnx)
        self.product_db_manager = ProductDBManager(self.cnx)
        self.category_db_manager = CategoryDBManager(self.cnx)

    def test_add_to_cart_success(self):
        self.category_db_manager.add_product_category('Product-category-1')

        validation = ProductUtil(self.cnx).validate_product_data(1, "Test Product", 199.00)
        self.assertTrue(True, validation)

        product_obj = self.product_db_manager.add_new_product(1, "Test Product", 199.00,
                                                              json.dumps({"suitable_for": "Test Demo"}))
        self.assertEqual(product_obj, "New product added successfully.")

        # add the newly created product in cart, 1 is the product id
        res = self.db_manager.add_product_into_cart(1)
        self.assertEqual(res, "Product added in cart.")

    def test_add_to_cart_fail(self):
        #  try to add the product, which is not in db
        res = self.db_manager.add_product_into_cart(2)
        self.assertEqual(res, "No product found.")

    def test_remove_product_from_cart_success(self):
        self.category_db_manager.add_product_category('Product-category-1')

        validation = ProductUtil(self.cnx).validate_product_data(1, "Test Product", 199.00)
        self.assertTrue(True, validation)

        product_obj = self.product_db_manager.add_new_product(1, "Test Product", 199.00,
                                                              json.dumps({"suitable_for": "Test Demo"}))
        self.assertEqual(product_obj, "New product added successfully.")

        # add the newly created product in cart, 1 is the product id
        res = self.db_manager.add_product_into_cart(1)
        self.assertEqual(res, "Product added in cart.")

        is_product_in_cart = self.db_manager.check_product_in_cart(1)
        self.assertTrue(is_product_in_cart)
        self.db_manager.remove_product_from_cart(1)

    def test_remove_product_from_cart_fail(self):
        # pass the product id which is not in db
        is_product_in_cart = self.db_manager.check_product_in_cart(2)
        self.assertFalse(is_product_in_cart)


if __name__ == "__main__":
    unittest.main()
