import json
import unittest

from my_cart_app.app.billing import BillingDBManager
from my_cart_app.app.cart import CartDBManager
from my_cart_app.app.categories import CategoryDBManager
from my_cart_app.app.product import ProductDBManager, ProductUtil
from test.test_db import MockDB


class TestBilling(MockDB):
    def setUp(self) -> None:
        self.db_manager = BillingDBManager(self.cnx)
        self.category_db_manager = CategoryDBManager(self.cnx)
        self.product_db_manager = ProductDBManager(self.cnx)
        self.cart_db_manager = CartDBManager(self.cnx)

    def test_bill_success(self):
        self.category_db_manager.add_product_category('Product-category-1')

        validation = ProductUtil(self.cnx).validate_product_data(1, "Test Product", 199.00)
        self.assertTrue(True, validation)

        product_obj = self.product_db_manager.add_new_product(1, "Test Product", 199.00,
                                                              json.dumps({"suitable_for": "Test Demo"}))
        self.assertEqual(product_obj, "New product added successfully.")

        # add the newly created product in cart, 1 is the product id
        res = self.cart_db_manager.add_product_into_cart(1)
        self.assertEqual(res, "Product added in cart.")

        # create the bill
        # here we are passing the customer name for billing
        res = self.db_manager.create_bill("Foo Bar")
        self.assertEqual(res, "Bill generated successfully.")

    def test_get_bills_generated_by_user(self):
        self.category_db_manager.add_product_category('Product-category-1')

        validation = ProductUtil(self.cnx).validate_product_data(1, "Test Product", 199.00)
        self.assertTrue(True, validation)

        product_obj = self.product_db_manager.add_new_product(1, "Test Product", 199.00,
                                                              json.dumps({"suitable_for": "Test Demo"}))
        self.assertEqual(product_obj, "New product added successfully.")

        # add the newly created product in cart, 1 is the product id
        res = self.cart_db_manager.add_product_into_cart(1)
        self.assertEqual(res, "Product added in cart.")

        # create the bill
        # here we are passing the customer name for billing
        res = self.db_manager.create_bill("Foo Bar")
        self.assertEqual(res, "Bill generated successfully.")

        response = self.db_manager.get_all_bills()
        self.assertEqual(len(response), 2)


if __name__ == "__main__":
    unittest.main()
