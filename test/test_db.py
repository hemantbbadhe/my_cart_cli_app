import mysql.connector
from unittest import TestCase

from my_cart_app import MYSQL_DB_NAME, MYSQL_USER, MYSQL_PASSWORD, MYSQL_HOST, MYSQL_PORT
from utils.database_queries import *
from utils.db_utils import connection, add_table


class MockDB(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.cnx = connection(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_PORT)
        cls.cursor = cls.cnx.cursor(dictionary=True)

        # drop database if it already exists
        try:
            cls.cursor.execute("DROP DATABASE {}".format(MYSQL_DB_NAME))
            cls.cursor.close()
            print("DB dropped")
        except mysql.connector.Error as err:
            # print("{}".format(MYSQL_DB))
            pass

        cls.cursor = cls.cnx.cursor(dictionary=True)
        try:
            cls.cursor.execute(
                "CREATE DATABASE {} DEFAULT CHARACTER SET 'utf8'".format(MYSQL_DB_NAME))
        except mysql.connector.Error as err:
            print("Failed creating database: {}".format(err))
            exit(1)
        cls.cnx.database = MYSQL_DB_NAME

        add_table(CATEGORIES_TABLE, cls.cnx)

        add_table(PRODUCTS_TABLE, cls.cnx)

        add_table(CART_TABLE, cls.cnx)

        add_table(BILLING_TABLE, cls.cnx)

    @classmethod
    def tearDownClass(cls):
        cnx = connection(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_PORT)
        cursor = cnx.cursor(dictionary=True)

        # drop test database
        try:
            cursor.execute("DROP DATABASE {}".format(MYSQL_DB_NAME))
            cnx.commit()
            cursor.close()
        except mysql.connector.Error as err:
            print("Database {} does not exists. Dropping db failed".format(MYSQL_DB_NAME))
        cnx.close()
