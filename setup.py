from setuptools import setup, find_packages

# A file to setup the entire project, it lists the packages that
# python needs to build and install the entire app as a package.
# It also serves as an entry point to specify the list of dependencies
# required by the application.
setup(
    name='my_cart_app',
    version='0.1',
    packages=find_packages(),
    install_requires=[]
)

"""
MYSQL_DB_NAME = "my_cart_app_db"
MYSQL_USER = "hemant"
MYSQL_PASSWORD = "Hemant@123"
MYSQL_HOST = "localhost"
MYSQL_PORT = "3306"

TEST_MYSQL_DB_NAME = "test_my_cart_app_db"
TEST_MYSQL_USER = "hemant"
TEST_MYSQL_PASSWORD = "Hemant@123"
TEST_MYSQL_HOST = "localhost"
TEST_MYSQL_PORT = "3306"

MAX_AMT_TO_DISCOUNT = 10000.00
DISCOUNT_AMT = 500.00

"""