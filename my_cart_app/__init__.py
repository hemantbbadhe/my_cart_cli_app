from my_cart_app.app_config import MYSQL_DB_NAME, MYSQL_USER, MYSQL_PASSWORD, MYSQL_HOST, MYSQL_PORT
from utils.database_queries import *
from utils.db_utils import connection, add_table

db_connection = connection(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_PORT)
cursor = db_connection.cursor()
cursor.execute("CREATE DATABASE IF NOT EXISTS {} DEFAULT CHARACTER SET 'utf8'".format(MYSQL_DB_NAME))
db_connection.database = MYSQL_DB_NAME

add_table(CATEGORIES_TABLE, db_connection)

add_table(PRODUCTS_TABLE, db_connection)

add_table(CART_TABLE, db_connection)

add_table(BILLING_TABLE, db_connection)
