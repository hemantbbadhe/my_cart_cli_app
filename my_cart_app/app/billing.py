import json

from my_cart_app import *
from my_cart_app.app_config import MAX_AMT_TO_DISCOUNT, DISCOUNT_AMT


class BillingManager:
    def __init__(self, connection):
        self.db_manager = BillingDBManager(connection)

    def create_bill(self):
        username = str(input("Enter name: "))
        return self.db_manager.create_bill(username)

    def get_all_bills(self):
        return self.db_manager.get_all_bills()


class BillingUtil:
    @staticmethod
    def get_cart_added_products_for_billing(db_cursor):
        columns = [col[0] for col in db_cursor.description]
        product_list = [dict(zip(columns, row)) for row in db_cursor.fetchall()]
        return product_list


class BillingDBManager:
    def __init__(self, connection):
        self.connection = connection
        self._util = BillingUtil

    def create_bill(self, username):
        query = f"SELECT p.id, p.productName, p.price FROM products p inner join cart c on p.id = c.productId;"
        cursor = self.connection.cursor()
        cursor.execute(query)

        products = self._util.get_cart_added_products_for_billing(cursor)

        if products:
            cart_amount = sum(item['price'] for item in products)
            discount = 0.00
            final_amount = cart_amount
            if cart_amount > MAX_AMT_TO_DISCOUNT:
                discount = DISCOUNT_AMT
                final_amount -= DISCOUNT_AMT

            insert_query = f"INSERT INTO billing (username, particulars, actual_amount, discount_amount, final_amount)" \
                           f" VALUES ('{username}', '{json.dumps(products)}', {cart_amount}, {discount}, {final_amount});"
            cursor.execute(insert_query)
            cursor.execute("delete from cart;")
            db_connection.commit()
            return "Bill generated successfully."
        return None

    def get_all_bills(self):
        query = f"SELECT * from billing;"
        cursor = self.connection.cursor()
        cursor.execute(query)

        all_bills = self._util.get_cart_added_products_for_billing(cursor)
        if not all_bills:
            return None
        return all_bills
