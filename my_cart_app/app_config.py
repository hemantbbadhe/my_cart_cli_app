MYSQL_DB_NAME = "my_cart_app_db"
MYSQL_USER = "<db_username>"
MYSQL_PASSWORD = "<db_password>"
MYSQL_HOST = "<db_host>"
MYSQL_PORT = "<db_port>"

TEST_MYSQL_DB_NAME = "test_my_cart_app_db"
TEST_MYSQL_USER = "<db_username>"
TEST_MYSQL_PASSWORD = "<db_password>"
TEST_MYSQL_HOST = "<db_host>"
TEST_MYSQL_PORT = "<db_port>"

MAX_AMT_TO_DISCOUNT = 10000.00
DISCOUNT_AMT = 500.00
